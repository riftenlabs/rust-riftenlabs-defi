//! # riftenlabs-defi
//!
//! `riftenlabs-defi` Collection of utilities for wortking with DeFi
//! contracts on Bitcoin Cash.
//!

#![deny(warnings)]
#![deny(missing_docs)]
#![deny(unsafe_code)]

/// The Cauldron contract CPMM contract
pub mod cauldron;
/// Miscellaneous helper functions
pub mod chainutil;
