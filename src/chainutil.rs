use std::slice::Iter;

use anyhow::{anyhow, bail, Context, Result};
use bitcoin_hashes::{hash_newtype, sha256, Hash, HashEngine};
use bitcoincash::{
    blockdata::{
        opcodes,
        script::{read_uint, Builder},
    },
    consensus::Encodable,
    Script, Txid,
};

hash_newtype!(
    OutPointHash,
    sha256::Hash,
    32,
    doc = "Hash pointing to a outpoint (utxo). The hash of txid & output index.",
    true
);

/// Compute a hash for transaction ID + vout index
pub fn compute_outpoint_hash(txid: &Txid, vout: u32) -> OutPointHash {
    let mut e = OutPointHash::engine();
    txid.consensus_encode(&mut e)
        .expect("failed to encode input txid");
    vout.consensus_encode(&mut e)
        .expect("failed to encode input index");
    OutPointHash::from_engine(e)
}

/// Check if a locking script is the result of given redeem script
pub fn is_p2sh32_of(redeem_script: &[u8], locking_script: &Script) -> bool {
    use bitcoin_hashes::sha256d;
    let mut e = sha256d::Hash::engine();
    e.input(redeem_script);
    let payload = sha256d::Hash::from_engine(e).to_vec();

    let expected = Builder::new()
        .push_opcode(opcodes::all::OP_HASH256)
        .push_slice(&payload)
        .push_opcode(opcodes::all::OP_EQUAL)
        .into_script();

    expected.eq(locking_script)
}

/// Get P2SH32 locking script given redeemscript
pub fn get_p2sh32_locking_script(redeemscript: &[u8]) -> Script {
    use bitcoin_hashes::sha256d;

    // Compute the SHA256 double hash of the redeem script to get the p2sh32 address payload
    let p2sh32_payload = sha256d::Hash::hash(redeemscript);

    // Create the p2sh32 locking script

    Builder::new()
        .push_opcode(opcodes::all::OP_HASH256)
        .push_slice(&p2sh32_payload)
        .push_opcode(opcodes::all::OP_EQUAL)
        .into_script()
}

/// Reads a item pushed in a script. Advances the iterator to after item and returns it.
/// Fails if the first byte is not a push operation.
/// Fails if there are fewer bytes left in iterator than indicated by push operation.
pub fn read_push_from_script(mut iter: Iter<u8>) -> Result<(Iter<u8>, Option<Vec<u8>>)> {
    macro_rules! take_stack_item {
        ($iter:expr, $len:expr) => {{
            let p = $iter.by_ref().take($len).cloned().collect::<Vec<u8>>();
            if p.len() < $len {
                return Err(anyhow!(
                    "Item on stack is smaller than expected ({} > {})",
                    p.len(),
                    $len
                ));
            }
            p
        }};
    }

    let opcode = if let Some(o) = iter.next() {
        opcodes::All::from(*o)
    } else {
        return Ok((iter, None));
    };

    if let opcodes::Class::PushBytes(n) = opcode.classify(opcodes::ClassifyContext::Legacy) {
        let item = Some(take_stack_item!(iter, n as usize));
        return Ok((iter, item));
    }
    if let opcodes::Class::PushNum(n) = opcode.classify(opcodes::ClassifyContext::Legacy) {
        return Ok((iter, Some((n as u8).to_be_bytes().to_vec())));
    }

    let n = match opcode {
        opcodes::all::OP_PUSHDATA1 => {
            // side effects: may write and break from the loop
            let n = read_uint(iter.as_slice(), 1).context("Invalid PUSHDATA1")?;
            iter.next().unwrap();
            n
        }
        opcodes::all::OP_PUSHDATA2 => {
            let n = read_uint(iter.as_slice(), 2).context("Invalid PUSHDATA2")?;
            iter.next().unwrap();
            iter.next().unwrap();
            n
        }
        opcodes::all::OP_PUSHDATA4 => {
            let n = read_uint(iter.as_slice(), 4).context("Invalid PUSHDATA4")?;
            iter.next().unwrap();
            iter.next().unwrap();
            iter.next().unwrap();
            iter.next().unwrap();
            n
        }
        _ => bail!("Not a push operation"),
    };
    let item = take_stack_item!(iter, n);
    Ok((iter, Some(item)))
}
